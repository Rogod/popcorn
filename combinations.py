# Combinations define what items can be combined to produce new
# items with an accompanying explanation
class Combination:
    def __init__(self,left,right,result,string,keepLeft,keepRight):
        self.left=left
        self.right=right
        self.result=result
        self.string=string
        self.keepLeft=keepLeft
        self.keepRight=keepRight

combinations=[]

combinations.append(Combination(0,1,3,"You squash the fish with the rock",True,False))
