# Actions define what item can be used on a particular map segment
# to change it to another map segment (e.g. hatchet used on lonely
# tree, changes it to cut down tree and keeps the hatchet)
# linkOn allows an action to trigger the next one regardless of
# parameters
class Action:
    def __init__(self,area,item,result,resultX,resultY,string,keepItem,linkOn):
        self.area=area
        self.item=item
        self.result=result
        self.resultX=resultX
        self.resultY=resultY
        self.string=string
        self.keepItem=keepItem
        self.linkOn=linkOn

actions=[]

actions.append(Action(7,2,8,31,15,"You chop down the poor dead tree",True,False))
actions.append(Action(9,4,4,31,23,"You unlock the gate",True,False))
actions.append(Action(9,4,10,31,22,"",True,False))
actions.append(Action(17,3,6,19,24,"You throw the fish guts near the bear",False,True))
actions.append(Action(17,3,19,20,24,"",False,False))
actions.append(Action(21,2,6,14,21,"You hack through the vines",True,True))
actions.append(Action(21,2,24,14,22,"",True,False))
actions.append(Action(23,6,18,12,21,"You lay the plank across the ravine",False,True))
actions.append(Action(23,6,25,13,21,"",False,False))
