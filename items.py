class Item:
    def __init__(self,name,x,y):
        self.name=name
        self.x=x
        self.y=y

items=[]

items.append(Item("rock",31,17))
items.append(Item("fish",33,18))
items.append(Item("hatchet",29,20))
items.append(Item("fishguts",-2,-2))
items.append(Item("key",28,20))
items.append(Item("spade",16,24))
items.append(Item("plank",14,24))
items.append(Item("bead",14,25))
