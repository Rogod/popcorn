places=[]
for x in range(64):
    places.append([])
p=1

#1
places[p].append("a farm")
places[p].append("This farm hasn't been used for some time")
p+=1

#2
places[p].append("the beach")
places[p].append("The sand is coarse and damp")
places[p].append("The tide has left debris strewn about the place")
p+=1

#3
places[p].append("a hut")
places[p].append("This was probably a woodsman's cabin")
p+=1

#4
places[p].append("a road")
places[p].append("A sturdy and unyielding road")
p+=1

#5
places[p].append("a gravel path")
places[p].append("This is a path well trodden")
p+=1

#6
places[p].append("a dirt track")
places[p].append("This path was probably a game trail once")
p+=1

#7
places[p].append("a lonely tree")
places[p].append("Surprising no one's chopped this tree down")
p+=1

#8
places[p].append("a freshly felled tree")
places[p].append("That tree won't cause you any more trouble now")
p+=1

#9
places[p].append("a large gate")
places[p].append("The lock is big and rusty")
p+=1

#10
places[p].append("a large open gate")
places[p].append("The path continues ahead of you")
p+=1

#11
places[p].append("a cave")
places[p].append("It's dark in here")
places[p].append("You can hear bats around you")
p+=1

#12
places[p].append("a farmyard")
places[p].append("It's really messy here")
p+=1

#13
places[p].append("a cliff")
places[p].append("Looks steep")
p+=1

#14
places[p].append("a gorge")
places[p].append("There's water trickling down here")
places[p].append("There are branches and rocks leading all the way up")
p+=1

#15
places[p].append("a field")
places[p].append("You can see the farmhouse down by the beach from up here")
places[p].append("Some cows are grazing")
p+=1

#16
places[p].append("a copse")
places[p].append("You are surrounded mostly by birch and oak trees")
p+=1

#17
places[p].append("a wounded bear")
places[p].append("The bear has lost sight in both eyes and can smell you")
p+=1

#18
places[p].append("a field")
places[p].append("Some sheep are lying in the grass")
p+=1

#19
places[p].append("a subdued bear")
places[p].append("The bear is trying to find the fish guts you threw near it")
p+=1

#20
places[p].append("a pit")
places[p].append("The ground has been disturbed recently")
p+=1

#21
places[p].append("some woody vines")
places[p].append("These vines have grown over the path")
places[p].append("Clearly this path has not been used in a long time")
p+=1

#22
places[p].append("a hovel")
places[p].append("There's no one home and the door is jammed shut")
p+=1

#23
places[p].append("a narrow ravine")
places[p].append("The edges are crumbly and sharp")
p+=1

#24
places[p].append("some broken vines")
places[p].append("Clearly this path has not been used in a long time")
p+=1

#25
places[p].append("a narrow ravine")
places[p].append("The edges are crumbly and sharp")
places[p].append("There is a plank across it")
p+=1

#63
places[63].append("END")
places[63].append("This is the temporary ending")
